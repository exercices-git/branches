# Exercice Branches

## Créer un nouveau dépôt

Cloner le dépôt créé dans l'exercice d'introduction dans un nouveau dépôt.

Ex : `git clone https://gitlab.com/exercices-git/correction-introduction.git`

## Ajouter créer une branche

Utiliser la commande checkout pour créer un nouvelle branche `feat/velo` en partant de `master`

## Ajouter des commit dans feat/velo

Créer un fichier `velo.yaml` dans le dossier `yaml` avec comme contenu :

```
velo:
  marque: Look
  modele: 576
```

Ajouter ce fichier à l'index et créer un commit avec le message `feat: velo.yaml`

## Merge Fast Forward

Placer vous sur la branche master

Visualiser votre historique pré-fusion avec `git log --oneline --graph --all`

Utiliser la commande `git merge --ff-only feat/velo` pour fusionner la branche `feat/velo` sur la branch `master`

Visualiser votre historique post-fusion avec `git log --oneline --graph --all`

## Merge Recursive

Défaire le dernier commit avec `git reset HEAD~1 --hard`

Visualiser votre historique pré-fusion avec `git log --oneline --graph --all`

Utiliser la commande `git merge --no-ff feat/velo` pour fusionner la branche `feat/velo` sur la branch `master`

Visualiser votre historique post-fusion avec `git log --oneline --graph --all`

## Merge par défault

Défaire le dernier commit avec `git reset HEAD~1 --hard`

Sur la branch `master` modifier le fichier `contact.yaml` en changeant par exemple le prénom.

Créer un commit `feat: fix prenom in contact.yaml` sur master

Visualiser votre historique pré-fusion avec `git log --oneline --graph --all`

Utiliser la commande `git merge feat/velo` pour fusionner la branche `feat/velo` sur la branch `master`

Visualiser votre historique post-fusion avec `git log --oneline --graph --all`

## Rebase

Défaire le dernier commit avec `git reset HEAD~1 --hard`

Visualiser votre historique pré-fusion avec `git log --oneline --graph --all`

Placer vous sur la branche `feat/velo` avec `git checkout feat/velo`

Utiliser la commande `git rebase master` pour réécrire l'historique de la branche `feat/velo` à partir de la branche `master`

Placer vous sur la branche `master` avec `git checkout master`

Utiliser la commande `git merge --ff-only feat/velo` pour fusionner la branche `feat/velo` sur la branch `master`

Visualiser votre historique post-fusion avec `git log --oneline --graph --all`

## Rebase preserve merge

Défaire les 2 derniers commit avec `git reset HEAD~1 --hard`

Utiliser la commande `git merge --no-ff feat/velo` pour fusionner la branche `feat/velo` sur la branch `master` avec la stratégie récursive

Visualiser votre historique pré-fusion avec `git log --oneline --graph --all`

Retourner dans la branch feat/velo 

Modifier le fichier vélo et faire un commit

Revenir sur master

Modifier le fichier contact et faire un commit

Utiliser la commande `git rebase --rebase-merges feat/velo` pour fusionner la branche `feat/velo` sur la branch `master`

Visualiser votre historique post-fusion avec `git log --oneline --graph --all`
